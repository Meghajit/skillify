import React, {Component} from 'react';
import Navbar from './components/Navbar';
import peopleImage from './resources/peopleImage.png'
import {Image} from "semantic-ui-react";

class App extends Component {
  render() {
    return (
      <div className="App">
          <Navbar className="navbar"/>
          <Image fluid src={peopleImage} style={{zIndex:'-1',  position:'fixed', filter: 'blur(5px)'}} />
          <div style={{textAlign: 'center', paddingTop: '10%'}}>
              <div className="name" style={{fontSize: '50px', color: '#fbfbfb'}}>Skillify</div>
              <div className="slogan" style={{marginTop:'33px', fontSize: '30px', color: '#ffffff'}}>Collaborate. Correlate. Succeed.</div>
          </div>
      </div>
    );
  }
}

export default App;