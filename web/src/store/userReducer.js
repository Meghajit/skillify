import {apiCallGet, apiCallPost} from "../apiCall";
import {push} from "react-router-redux";

export const ADD_USER = 'ADD_USER';
export const PATCH_USER = 'PATCH_USER';
export const DELETE_USER = 'DELETE_USER';

const initState ={
    userId: '',
    password: ''
};

//reducer function
const userReducer = (state=initState, action) => {
    switch(action.type) {
        case ADD_USER :
            return {
                ...state,
                userId: action.data.userId,
                password: action.data.password
            };
        case PATCH_USER :
            return {
                ...state,
                userId: action.data.userId
            };
        case DELETE_USER :
            return initState;
        default :
            return state
    }
};

export const onLogin = (dispatch, hid, password) => {

    const getUserIdPromise = apiCallPost("/api/user/auth", {userId: hid, password: password});
    getUserIdPromise.then((result) => {
        apiCallGet("/api/user/"+result.id).then((user)=> {
            dispatch({type:ADD_USER, data:user});
            dispatch(push('/user/'+user.id+'/dashboard'));
        }, (err)=> {console.log("ERROR"+err)});
    }, (err) => {
        console.log("ERROR" + err)
    });
};

export default userReducer;



