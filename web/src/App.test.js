import React from 'react';
import App from './App';
import {shallow} from 'enzyme';

describe('<App />', () => {
    it('renders', () => {
        const wrapper = shallow(<App />);
        expect(wrapper).toContainComponentWithProps('.name', {children: 'Skillify'});
        expect(wrapper).toContainComponentWithProps('.slogan', {children: 'Collaborate. Correlate. Succeed.'});
        expect(wrapper).toContainComponent('.navbar');
    });
});