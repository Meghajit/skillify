import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import _ from 'lodash';

configure({ adapter: new Adapter() });

expect.extend({
    toContainComponent(parent, expectedChild) {
        const pass = parent.find(expectedChild).length > 0;
        if (pass) {
            return {
                message: () => (
                    `expected\n\t${parent.debug()}\nnot to contain\n\t${expectedChild}`
                ),
                pass: true,
            };
        } else {
            return {
                message: () => (`expected\n\t${parent.debug()}\n to contain\n\t${expectedChild}`),
                pass: false,
            };
        }
    },
    toContainComponentWithProps(parent, expectedChild, expectedProps) {
        const candidates = parent.find(expectedChild);
        const matching = candidates.filterWhere((c) => {
            return _.every(expectedProps, (value, key) => {
                return _.isEqual(c.prop(key), value);
            });
        });
        const candidatesString = candidates.map((c) => {
            const result = _.pick(c.props(), _.keys(expectedProps));
            return JSON.stringify(result);
        }).join('\n');
        const pass = matching.length > 0;
        if (pass) {
            return {
                message: () => (
                    `expected\n\t${parent.debug()}\nnot to contain\n\t${expectedChild}\nwith props\n\t${JSON.stringify(expectedProps)}\nFound ${candidates.length} possible matches with props:\n\t${candidatesString}`
                ),
                pass: true,
            };
        } else {
            return {
                message: () => (
                    `expected\n\t${parent.debug()}\n to contain\n\t${expectedChild}\nwith props\n\t${JSON.stringify(expectedProps)}\nFound ${candidates.length} possible matches with props:\n\t${candidatesString}`
                ),
                pass: false,
            };
        }
    },
});