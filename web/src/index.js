import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import {Route} from 'react-router-dom';
import {ConnectedRouter, routerMiddleware, routerReducer} from 'react-router-redux';
import logger from 'redux-logger'
import createHistory from 'history/createBrowserHistory'
import './index.css';
import 'semantic-ui-css/semantic.min.css';
import registerServiceWorker from './registerServiceWorker';
import App from './App';
import userReducer from "./store/userReducer";
import DashboardContainer from "./containers/DashboardContainer";

const history = createHistory();
const middleware = routerMiddleware(history);
const store = createStore(
    combineReducers({
        user:userReducer,
        router: routerReducer,
    }),
    applyMiddleware(middleware,logger)
);
ReactDOM.render(
    <Provider store ={store}>
        <ConnectedRouter history={history}>
            <div>
                <Route exact path="/" component={App}/>
                <Route path='/user/:userId/dashboard' component={DashboardContainer}/>
            </div>
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
