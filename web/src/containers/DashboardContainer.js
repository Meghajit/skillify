import {connect} from "react-redux";
import Dashboard from "../components/Dashboard";

const mapStateToProps = (state, ownProps) => {
    console.log("State----"+JSON.stringify(state));
    return {
        ...ownProps,
        userId:state.user.userId,
    }
};
export default connect(mapStateToProps)(Dashboard)