import LoginDetails from '../components/LoginDetails'
import {onLogin} from '../store/userReducer'
import {connect} from 'react-redux';

const mapDispatchToProps =(dispatch) => {
    return {
        onSubmit: (hid, password) => {
            onLogin(dispatch, hid, password);
        }
    }
};

export default connect(null,mapDispatchToProps)(LoginDetails);
