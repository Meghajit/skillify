let hostname = 'http://localhost:8080';
if (window.location.hostname === 'skillify-web.cfapps.io') {
    hostname = 'https://skillify-api.cfapps.io'
}

export const apiCallGet = (url) => {

    return new Promise((resolve, reject) => {
        fetch(hostname + url, {
            method: 'GET',
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
        .then(res => res.json(), (err) => {
            reject(err)
        })
        .then((response) => {resolve(response)}, (err) => {
            reject(err)
        })
    });
};

export const apiCallPost = (url, data) => {

    return new Promise((resolve, reject) => {
        fetch(hostname + url, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
        .then(res => res.json(), (err) => {
            reject(err)
        })
        .then((response) => {resolve(response)}, (err) => {
            reject(err)
        })
    });
};
