import React, {Component} from 'react';
import {Button, Form} from 'semantic-ui-react';

class LoginDetails extends Component {

    constructor(props){
        super(props);
        this.state ={
            hid:null,
            password:null,
        }
    }

    handleHidChange = (value) => {
        this.setState({hid:value});
};
    handlePasswordChange = (value) => {
        this.setState({password:value});
    };

    onSubmitHandler() {
        const {onSubmit} = this.props;
        const {hid,password} = this.state;
        onSubmit(hid, password);
    };

    render() {
        const {hid,password} = this.state;
        return (
            <div>
                <Form>
                    <Form.Field>
                        <label>HID</label>
                        <input className="hid" onChange ={(e) => this.handleHidChange(e.target.value)} required placeholder='123456' />
                    </Form.Field>
                    <Form.Field>
                        <label>Password</label>
                        <input className="password" onChange ={(e) => this.handlePasswordChange(e.target.value)} required placeholder='usertesting' />
                    </Form.Field>
                    <Button className="submit"
                            onClick ={() => this.onSubmitHandler()}
                            primary
                            disabled = {!Boolean(hid&&password)}>Login</Button>
                </Form>
            </div>
        );
    }
}

export default LoginDetails;