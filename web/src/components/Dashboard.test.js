import Dashboard from "./Dashboard";
import {shallow} from "enzyme";
import React from 'react';

const props = {
  userId:'Meghajit',
  password:'abc'
};
describe('<Dashboard/>', () => {

    it('renders the welcome message with User Id', () => {
        const wrapper = shallow(<Dashboard {...props}/>);
        expect(wrapper.find('.welcome-message').text()).toEqual('Hey Meghajit ! Welcome to your Dashboard');
    })
});