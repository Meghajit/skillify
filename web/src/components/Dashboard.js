import React, {Component} from 'react';

class Dashboard extends Component {
    render() {
        const {userId} = this.props;
        return (
            <h1 className="welcome-message">Hey {userId} ! Welcome to your Dashboard</h1>
        );
    };
}

export default Dashboard;