import React, {Component} from 'react';
import {Button, Header, Image, Menu, Modal} from "semantic-ui-react";
import skillifyIcon from '../resources/skillifyIcon.png';
import dev from '../resources/dev.png';
import LoginDetailsContainer from '../containers/LoginDetailsContainer';

class Navbar extends Component {

    render(){
        return(
            <div>
            <Menu inverted pointing >
                <span className="skillifyIcon"><Image src={skillifyIcon} /></span>
                <Menu.Menu position='right'>
                    <Menu.Item>
                        <Modal className="login-modal" style={{marginTop:'0', position: 'absolute', left: '23%', top: '11%'}}
                               size='small'
                               trigger={<Button className="login" primary> Login </Button>}>
                            <Modal.Header>Login</Modal.Header>
                            <Modal.Content>
                                <Modal.Description >
                                    <LoginDetailsContainer/>
                                </Modal.Description>
                            </Modal.Content>
                        </Modal>
                    </Menu.Item>
                    <Menu.Item>
                       <Modal className="about-modal" style={{marginTop:'0', position: 'absolute', left: '23%', top: '11%'}}
                               size='small'
                               trigger={<Button className="about" primary> About </Button>}>
                            <Modal.Header>About</Modal.Header>
                            <Modal.Content image>
                                <Image wrapped size='medium' src={dev} />
                                <Modal.Description>
                                    <Header>Meghajit Mazumdar</Header>
                                    <p>Fullstack Developer</p>
                                </Modal.Description>
                            </Modal.Content>
                        </Modal>
                    </Menu.Item>
                </Menu.Menu>
            </Menu>
            </div>
        );
    }
}

export default Navbar;