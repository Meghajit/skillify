import LoginDetails from './LoginDetails';
import React from 'react';
import {shallow} from 'enzyme';

const props ={
    onSubmit : ()=> {},
}

describe('<LoginDetails />', () => {
    it('renders a login form', () => {
        const wrapper = shallow(<LoginDetails {...props} />);
        expect(wrapper).toContainComponent('.hid');
        expect(wrapper).toContainComponent('.password');
        expect(wrapper).toContainComponent('.submit');
    });

    it('clicking submit button calls onSubmitHandler', ()=> {
        const onSubmit = jest.fn();
        const wrapper = shallow(<LoginDetails {...props} onSubmit={onSubmit}/>);
        wrapper.find('.hid').simulate('change', {target: {value: '324828'}});
        wrapper.find('.password').simulate('change', {target: {value: 'kasdjkla'}});
        wrapper.find('.submit').simulate('click');
        expect(onSubmit).toHaveBeenCalledWith('324828','kasdjkla');

    });

    it('submit button is disabled until both the details are filled', () => {
        const wrapper = shallow(<LoginDetails {...props}/>);
        expect(wrapper).toContainComponentWithProps('.submit',{disabled:true});
        wrapper.find('.hid').simulate('change',{target:{value:'h123456'}});
        expect(wrapper).toContainComponentWithProps('.submit',{disabled:true});
        wrapper.find('.password').simulate('change',{target:{value:'jdsdsadna'}});
        expect(wrapper).toContainComponentWithProps('.submit',{disabled:false});
    });
});