import React from 'react';
import Navbar from './Navbar'
import {shallow} from 'enzyme';

describe('<Navbar />', () => {
    it('renders the login modal, about modal and icon', () => {
        const wrapper = shallow(<Navbar/>);
        expect(wrapper).toContainComponent('Connect(LoginDetails)');
        expect(wrapper).toContainComponent('.skillifyIcon');
        expect(wrapper).toContainComponent('.about-modal');
    });
});