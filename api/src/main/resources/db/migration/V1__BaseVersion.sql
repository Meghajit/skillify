create type dblink_pkey_results as
(
	position integer,
	colname text
)
;

create type tablefunc_crosstab_2 as
(
	row_name text,
	category_1 text,
	category_2 text
)
;

create type tablefunc_crosstab_3 as
(
	row_name text,
	category_1 text,
	category_2 text,
	category_3 text
)
;

create type tablefunc_crosstab_4 as
(
	row_name text,
	category_1 text,
	category_2 text,
	category_3 text,
	category_4 text
)
;

create domain earth as cube
	constraint not_3d check (cube_dim(VALUE) <= 3)
	constraint not_point check cube_is_point(VALUE)
	constraint on_surface check (abs(((cube_distance(VALUE, '(0)'::cube) / earth()) - (1)::double precision)) < '1e-06'::double precision)
;

CREATE VIEW pg_stat_statements AS SELECT pg_stat_statements.userid,
    pg_stat_statements.dbid,
    pg_stat_statements.queryid,
    pg_stat_statements.query,
    pg_stat_statements.calls,
    pg_stat_statements.total_time,
    pg_stat_statements.min_time,
    pg_stat_statements.max_time,
    pg_stat_statements.mean_time,
    pg_stat_statements.stddev_time,
    pg_stat_statements.rows,
    pg_stat_statements.shared_blks_hit,
    pg_stat_statements.shared_blks_read,
    pg_stat_statements.shared_blks_dirtied,
    pg_stat_statements.shared_blks_written,
    pg_stat_statements.local_blks_hit,
    pg_stat_statements.local_blks_read,
    pg_stat_statements.local_blks_dirtied,
    pg_stat_statements.local_blks_written,
    pg_stat_statements.temp_blks_read,
    pg_stat_statements.temp_blks_written,
    pg_stat_statements.blk_read_time,
    pg_stat_statements.blk_write_time
   FROM pg_stat_statements(true) pg_stat_statements(userid, dbid, queryid, query, calls, total_time, min_time, max_time, mean_time, stddev_time, rows, shared_blks_hit, shared_blks_read, shared_blks_dirtied, shared_blks_written, local_blks_hit, local_blks_read, local_blks_dirtied, local_blks_written, temp_blks_read, temp_blks_written, blk_read_time, blk_write_time)
;

create function xpath_list(text, text) returns text
	immutable
	language sql
as $$
SELECT xpath_list($1,$2,',')
$$
;

create function xpath_nodeset(text, text) returns text
	immutable
	language sql
as $$
SELECT xpath_nodeset($1,$2,'','')
$$
;

create function xpath_nodeset(text, text, text) returns text
	immutable
	language sql
as $$
SELECT xpath_nodeset($1,$2,'',$3)
$$
;

create function regexp_matches(citext, citext) returns SETOF text[]
	immutable
	language sql
as $$
SELECT pg_catalog.regexp_matches( $1::pg_catalog.text, $2::pg_catalog.text, 'i' );
$$
;

create function regexp_matches(citext, citext, text) returns SETOF text[]
	immutable
	language sql
as $$
SELECT pg_catalog.regexp_matches( $1::pg_catalog.text, $2::pg_catalog.text, CASE WHEN pg_catalog.strpos($3, 'c') = 0 THEN  $3 || 'i' ELSE $3 END );
$$
;

create function regexp_replace(citext, citext, text) returns text
	immutable
	language sql
as $$
SELECT pg_catalog.regexp_replace( $1::pg_catalog.text, $2::pg_catalog.text, $3, 'i');
$$
;

create function regexp_replace(citext, citext, text, text) returns text
	immutable
	language sql
as $$
SELECT pg_catalog.regexp_replace( $1::pg_catalog.text, $2::pg_catalog.text, $3, CASE WHEN pg_catalog.strpos($4, 'c') = 0 THEN  $4 || 'i' ELSE $4 END);
$$
;

create function regexp_split_to_array(citext, citext) returns text[]
	immutable
	language sql
as $$
SELECT pg_catalog.regexp_split_to_array( $1::pg_catalog.text, $2::pg_catalog.text, 'i' );
$$
;

create function regexp_split_to_array(citext, citext, text) returns text[]
	immutable
	language sql
as $$
SELECT pg_catalog.regexp_split_to_array( $1::pg_catalog.text, $2::pg_catalog.text, CASE WHEN pg_catalog.strpos($3, 'c') = 0 THEN  $3 || 'i' ELSE $3 END );
$$
;

create function regexp_split_to_table(citext, citext) returns SETOF text
	immutable
	language sql
as $$
SELECT pg_catalog.regexp_split_to_table( $1::pg_catalog.text, $2::pg_catalog.text, 'i' );
$$
;

create function regexp_split_to_table(citext, citext, text) returns SETOF text
	immutable
	language sql
as $$
SELECT pg_catalog.regexp_split_to_table( $1::pg_catalog.text, $2::pg_catalog.text, CASE WHEN pg_catalog.strpos($3, 'c') = 0 THEN  $3 || 'i' ELSE $3 END );
$$
;

create function strpos(citext, citext) returns integer
	immutable
	language sql
as $$
SELECT pg_catalog.strpos( pg_catalog.lower( $1::pg_catalog.text ), pg_catalog.lower( $2::pg_catalog.text ) );
$$
;

create function replace(citext, citext, citext) returns text
	immutable
	language sql
as $$
SELECT pg_catalog.regexp_replace( $1::pg_catalog.text, pg_catalog.regexp_replace($2::pg_catalog.text, '([^a-zA-Z_0-9])', E'\\\\\\1', 'g'), $3::pg_catalog.text, 'gi' );
$$
;

create function split_part(citext, citext, integer) returns text
	immutable
	language sql
as $$
SELECT (pg_catalog.regexp_split_to_array( $1::pg_catalog.text, pg_catalog.regexp_replace($2::pg_catalog.text, '([^a-zA-Z_0-9])', E'\\\\\\1', 'g'), 'i'))[$3];
$$
;

create function translate(citext, citext, text) returns text
	immutable
	language sql
as $$
SELECT pg_catalog.translate( pg_catalog.translate( $1::pg_catalog.text, pg_catalog.lower($2::pg_catalog.text), $3), pg_catalog.upper($2::pg_catalog.text), $3);
$$
;

create function earth() returns double precision
	immutable
	language sql
as $$
SELECT '6378168'::float8
$$
;

create function sec_to_gc(double precision) returns double precision
	immutable
	language sql
as $$
SELECT CASE WHEN $1 < 0 THEN 0::float8 WHEN $1/(2*earth()) > 1 THEN pi()*earth() ELSE 2*earth()*asin($1/(2*earth())) END
$$
;

create function gc_to_sec(double precision) returns double precision
	immutable
	language sql
as $$
SELECT CASE WHEN $1 < 0 THEN 0::float8 WHEN $1/earth() > pi() THEN 2*earth() ELSE 2*earth()*sin($1/(2*earth())) END
$$
;

create function ll_to_earth(double precision, double precision) returns earth
	immutable
	language sql
as $$
SELECT cube(cube(cube(earth()*cos(radians($1))*cos(radians($2))),earth()*cos(radians($1))*sin(radians($2))),earth()*sin(radians($1)))::earth
$$
;

create function latitude(earth) returns double precision
	immutable
	language sql
as $$
SELECT CASE WHEN cube_ll_coord($1, 3)/earth() < -1 THEN -90::float8 WHEN cube_ll_coord($1, 3)/earth() > 1 THEN 90::float8 ELSE degrees(asin(cube_ll_coord($1, 3)/earth())) END
$$
;

create function longitude(earth) returns double precision
	immutable
	language sql
as $$
SELECT degrees(atan2(cube_ll_coord($1, 2), cube_ll_coord($1, 1)))
$$
;

create function earth_distance(earth, earth) returns double precision
	immutable
	language sql
as $$
SELECT sec_to_gc(cube_distance($1, $2))
$$
;

create function earth_box(earth, double precision) returns cube
	immutable
	language sql
as $$
SELECT cube_enlarge($1, gc_to_sec($2), 3)
$$
;

create operator > (procedure = ltree_gt, leftarg = ltree, rightarg = ltree)
;

create operator >= (procedure = ltree_ge, leftarg = ltree, rightarg = ltree)
;

create operator < (procedure = ltree_lt, leftarg = ltree, rightarg = ltree)
;

create operator <= (procedure = ltree_le, leftarg = ltree, rightarg = ltree)
;

create operator <> (procedure = ltree_ne, leftarg = ltree, rightarg = ltree)
;

create operator = (procedure = ltree_eq, leftarg = ltree, rightarg = ltree)
;

create operator <@ (procedure = ltree_risparent, leftarg = ltree, rightarg = ltree)
;

create operator @> (procedure = ltree_isparent, leftarg = ltree, rightarg = ltree)
;

create operator ^<@ (procedure = ltree_risparent, leftarg = ltree, rightarg = ltree)
;

create operator ^@> (procedure = ltree_isparent, leftarg = ltree, rightarg = ltree)
;

create operator || (procedure = ltree_addltree, leftarg = ltree, rightarg = ltree)
;

create operator || (procedure = ltree_addtext, leftarg = ltree, rightarg = text)
;

create operator || (procedure = ltree_textadd, leftarg = text, rightarg = ltree)
;

create operator ~ (procedure = ltq_rregex, leftarg = lquery, rightarg = ltree)
;

create operator ~ (procedure = ltq_regex, leftarg = ltree, rightarg = lquery)
;

create operator ^~ (procedure = ltq_rregex, leftarg = lquery, rightarg = ltree)
;

create operator ^~ (procedure = ltq_regex, leftarg = ltree, rightarg = lquery)
;

create operator ? (procedure = lt_q_rregex, leftarg = lquery[], rightarg = ltree)
;

create operator ? (procedure = lt_q_regex, leftarg = ltree, rightarg = lquery[])
;

create operator ^? (procedure = lt_q_rregex, leftarg = lquery[], rightarg = ltree)
;

create operator ^? (procedure = lt_q_regex, leftarg = ltree, rightarg = lquery[])
;

create operator @ (procedure = ltxtq_rexec, leftarg = ltxtquery, rightarg = ltree)
;

create operator @ (procedure = ltxtq_exec, leftarg = ltree, rightarg = ltxtquery)
;

create operator ^@ (procedure = ltxtq_rexec, leftarg = ltxtquery, rightarg = ltree)
;

create operator ^@ (procedure = ltxtq_exec, leftarg = ltree, rightarg = ltxtquery)
;

create operator <@ (procedure = "_ltree_r_isparent", leftarg = ltree, rightarg = ltree[])
;

create operator @> (procedure = "_ltree_isparent", leftarg = ltree[], rightarg = ltree)
;

create operator @> (procedure = "_ltree_r_risparent", leftarg = ltree, rightarg = ltree[])
;

create operator <@ (procedure = "_ltree_risparent", leftarg = ltree[], rightarg = ltree)
;

create operator ~ (procedure = "_ltq_rregex", leftarg = lquery, rightarg = ltree[])
;

create operator ~ (procedure = "_ltq_regex", leftarg = ltree[], rightarg = lquery)
;

create operator ? (procedure = "_lt_q_rregex", leftarg = lquery[], rightarg = ltree[])
;

create operator ? (procedure = "_lt_q_regex", leftarg = ltree[], rightarg = lquery[])
;

create operator @ (procedure = "_ltxtq_rexec", leftarg = ltxtquery, rightarg = ltree[])
;

create operator @ (procedure = "_ltxtq_exec", leftarg = ltree[], rightarg = ltxtquery)
;

create operator ^<@ (procedure = "_ltree_r_isparent", leftarg = ltree, rightarg = ltree[])
;

create operator ^@> (procedure = "_ltree_isparent", leftarg = ltree[], rightarg = ltree)
;

create operator ^@> (procedure = "_ltree_r_risparent", leftarg = ltree, rightarg = ltree[])
;

create operator ^<@ (procedure = "_ltree_risparent", leftarg = ltree[], rightarg = ltree)
;

create operator ^~ (procedure = "_ltq_rregex", leftarg = lquery, rightarg = ltree[])
;

create operator ^~ (procedure = "_ltq_regex", leftarg = ltree[], rightarg = lquery)
;

create operator ^? (procedure = "_lt_q_rregex", leftarg = lquery[], rightarg = ltree[])
;

create operator ^? (procedure = "_lt_q_regex", leftarg = ltree[], rightarg = lquery[])
;

create operator ^@ (procedure = "_ltxtq_rexec", leftarg = ltxtquery, rightarg = ltree[])
;

create operator ^@ (procedure = "_ltxtq_exec", leftarg = ltree[], rightarg = ltxtquery)
;

create operator ?@> (procedure = "_ltree_extract_isparent", leftarg = ltree[], rightarg = ltree)
;

create operator ?<@ (procedure = "_ltree_extract_risparent", leftarg = ltree[], rightarg = ltree)
;

create operator ?~ (procedure = "_ltq_extract_regex", leftarg = ltree[], rightarg = lquery)
;

create operator ?@ (procedure = "_ltxtq_extract_exec", leftarg = ltree[], rightarg = ltxtquery)
;

create operator <> (procedure = citext_ne, leftarg = citext, rightarg = citext)
;

create operator = (procedure = citext_eq, leftarg = citext, rightarg = citext)
;

create operator > (procedure = citext_gt, leftarg = citext, rightarg = citext)
;

create operator >= (procedure = citext_ge, leftarg = citext, rightarg = citext)
;

create operator < (procedure = citext_lt, leftarg = citext, rightarg = citext)
;

create operator <= (procedure = citext_le, leftarg = citext, rightarg = citext)
;

create operator !~ (procedure = "public.texticregexne", leftarg = citext, rightarg = citext)
;

create operator ~ (procedure = "public.texticregexeq", leftarg = citext, rightarg = citext)
;

create operator !~* (procedure = "public.texticregexne", leftarg = citext, rightarg = citext)
;

create operator ~* (procedure = "public.texticregexeq", leftarg = citext, rightarg = citext)
;

create operator !~~ (procedure = "public.texticnlike", leftarg = citext, rightarg = citext)
;

create operator ~~ (procedure = "public.texticlike", leftarg = citext, rightarg = citext)
;

create operator !~~* (procedure = "public.texticnlike", leftarg = citext, rightarg = citext)
;

create operator ~~* (procedure = "public.texticlike", leftarg = citext, rightarg = citext)
;

create operator !~ (procedure = "public.texticregexne", leftarg = citext, rightarg = text)
;

create operator ~ (procedure = "public.texticregexeq", leftarg = citext, rightarg = text)
;

create operator !~* (procedure = "public.texticregexne", leftarg = citext, rightarg = text)
;

create operator ~* (procedure = "public.texticregexeq", leftarg = citext, rightarg = text)
;

create operator !~~ (procedure = "public.texticnlike", leftarg = citext, rightarg = text)
;

create operator ~~ (procedure = "public.texticlike", leftarg = citext, rightarg = text)
;

create operator !~~* (procedure = "public.texticnlike", leftarg = citext, rightarg = text)
;

create operator ~~* (procedure = "public.texticlike", leftarg = citext, rightarg = text)
;

create operator % (procedure = similarity_op, leftarg = text, rightarg = text)
;

create operator %> (procedure = word_similarity_commutator_op, leftarg = text, rightarg = text)
;

create operator <% (procedure = word_similarity_op, leftarg = text, rightarg = text)
;

create operator <-> (procedure = similarity_dist, leftarg = text, rightarg = text)
;

create operator <->> (procedure = word_similarity_dist_commutator_op, leftarg = text, rightarg = text)
;

create operator <<-> (procedure = word_similarity_dist_op, leftarg = text, rightarg = text)
;

create operator ~~ (procedure = rboolop, leftarg = query_int, rightarg = integer[])
;

create operator @@ (procedure = boolop, leftarg = integer[], rightarg = query_int)
;

create operator && (procedure = "_int_overlap", leftarg = integer[], rightarg = integer[])
;

create operator <@ (procedure = "_int_contained", leftarg = integer[], rightarg = integer[])
;

create operator @> (procedure = "_int_contains", leftarg = integer[], rightarg = integer[])
;

create operator ~ (procedure = "_int_contained", leftarg = integer[], rightarg = integer[])
;

create operator @ (procedure = "_int_contains", leftarg = integer[], rightarg = integer[])
;

create operator # (procedure = icount, rightarg = integer[])
;

create operator # (procedure = idx, leftarg = integer[], rightarg = integer)
;

create operator + (procedure = intarray_push_elem, leftarg = integer[], rightarg = integer)
;

create operator + (procedure = intarray_push_array, leftarg = integer[], rightarg = integer[])
;

create operator - (procedure = intarray_del_elem, leftarg = integer[], rightarg = integer)
;

create operator | (procedure = intset_union_elem, leftarg = integer[], rightarg = integer)
;

create operator | (procedure = "_int_union", leftarg = integer[], rightarg = integer[])
;

create operator - (procedure = intset_subtract, leftarg = integer[], rightarg = integer[])
;

create operator & (procedure = "_int_inter", leftarg = integer[], rightarg = integer[])
;

create operator -> (procedure = fetchval, leftarg = hstore, rightarg = text)
;

create operator -> (procedure = slice_array, leftarg = hstore, rightarg = text[])
;

create operator ? (procedure = exist, leftarg = hstore, rightarg = text)
;

create operator ?| (procedure = exists_any, leftarg = hstore, rightarg = text[])
;

create operator ?& (procedure = exists_all, leftarg = hstore, rightarg = text[])
;

create operator - (procedure = "public.delete", leftarg = hstore, rightarg = text)
;

create operator - (procedure = "public.delete", leftarg = hstore, rightarg = text[])
;

create operator - (procedure = "public.delete", leftarg = hstore, rightarg = hstore)
;

create operator || (procedure = hs_concat, leftarg = hstore, rightarg = hstore)
;

create operator <@ (procedure = hs_contained, leftarg = hstore, rightarg = hstore)
;

create operator @> (procedure = hs_contains, leftarg = hstore, rightarg = hstore)
;

create operator ~ (procedure = hs_contained, leftarg = hstore, rightarg = hstore)
;

create operator @ (procedure = hs_contains, leftarg = hstore, rightarg = hstore)
;

create operator %% (procedure = hstore_to_array, rightarg = hstore)
;

create operator %# (procedure = hstore_to_matrix, rightarg = hstore)
;

create operator #= (procedure = populate_record, leftarg = anyelement, rightarg = hstore)
;

create operator <> (procedure = hstore_ne, leftarg = hstore, rightarg = hstore)
;

create operator = (procedure = hstore_eq, leftarg = hstore, rightarg = hstore)
;

create operator #># (procedure = hstore_gt, leftarg = hstore, rightarg = hstore)
;

create operator #>=# (procedure = hstore_ge, leftarg = hstore, rightarg = hstore)
;

create operator #<# (procedure = hstore_lt, leftarg = hstore, rightarg = hstore)
;

create operator #<=# (procedure = hstore_le, leftarg = hstore, rightarg = hstore)
;

create operator > (procedure = cube_gt, leftarg = cube, rightarg = cube)
;

create operator >= (procedure = cube_ge, leftarg = cube, rightarg = cube)
;

create operator < (procedure = cube_lt, leftarg = cube, rightarg = cube)
;

create operator <= (procedure = cube_le, leftarg = cube, rightarg = cube)
;

create operator && (procedure = cube_overlap, leftarg = cube, rightarg = cube)
;

create operator <> (procedure = cube_ne, leftarg = cube, rightarg = cube)
;

create operator = (procedure = cube_eq, leftarg = cube, rightarg = cube)
;

create operator <@ (procedure = cube_contained, leftarg = cube, rightarg = cube)
;

create operator @> (procedure = cube_contains, leftarg = cube, rightarg = cube)
;

create operator -> (procedure = cube_coord, leftarg = cube, rightarg = integer)
;

create operator ~> (procedure = cube_coord_llur, leftarg = cube, rightarg = integer)
;

create operator <#> (procedure = distance_taxicab, leftarg = cube, rightarg = cube)
;

create operator <-> (procedure = cube_distance, leftarg = cube, rightarg = cube)
;

create operator <=> (procedure = distance_chebyshev, leftarg = cube, rightarg = cube)
;

create operator ~ (procedure = cube_contained, leftarg = cube, rightarg = cube)
;

create operator @ (procedure = cube_contains, leftarg = cube, rightarg = cube)
;

create operator <@> (procedure = geo_distance, leftarg = point, rightarg = point)
;

create operator <-> (procedure = cash_dist, leftarg = money, rightarg = money)
;

create operator <-> (procedure = date_dist, leftarg = date, rightarg = date)
;

create operator <-> (procedure = float4_dist, leftarg = real, rightarg = real)
;

create operator <-> (procedure = float8_dist, leftarg = double precision, rightarg = double precision)
;

create operator <-> (procedure = int2_dist, leftarg = smallint, rightarg = smallint)
;

create operator <-> (procedure = int4_dist, leftarg = integer, rightarg = integer)
;

create operator <-> (procedure = int8_dist, leftarg = bigint, rightarg = bigint)
;

create operator <-> (procedure = interval_dist, leftarg = interval, rightarg = interval)
;

create operator <-> (procedure = oid_dist, leftarg = oid, rightarg = oid)
;

create operator <-> (procedure = time_dist, leftarg = time, rightarg = time)
;

create operator <-> (procedure = ts_dist, leftarg = timestamp, rightarg = timestamp)
;

create operator <-> (procedure = tstz_dist, leftarg = timestamp with time zone, rightarg = timestamp with time zone)
;