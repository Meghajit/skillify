package com.achintya.skillify

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SkillifyApplication

fun main(args: Array<String>) {
    runApplication<SkillifyApplication>(*args)
}
