package com.achintya.skillify.user

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface UserRepository:JpaRepository<Users, Int> {

    fun findByUserId(userId: String?): List<Users>

    @Query("Select id from Users where userId = :userId  and password = :password")
    fun validateUser(@Param("userId") userId: String?, @Param("password") password: String?): Int?
}