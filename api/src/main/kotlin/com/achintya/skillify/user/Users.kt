package com.achintya.skillify.user
import javax.persistence.*
import javax.persistence.GenerationType.SEQUENCE

@Entity
data class Users(
    @SequenceGenerator(name="USERS_SEQUENCE_GENERATOR", sequenceName="users_id_seq", initialValue = 1, allocationSize = 1)
    @Id @GeneratedValue(strategy= SEQUENCE, generator="USERS_SEQUENCE_GENERATOR")
    val id: Int?=null,

    @Column(name = "userid")
    val userId: String?=null,
    val password: String?=null
)