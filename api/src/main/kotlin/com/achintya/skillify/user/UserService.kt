package com.achintya.skillify.user

import org.springframework.stereotype.Service
import java.util.*

@Service
class UserService(val userRepository: UserRepository) {

    fun addUser(user:Users): Users? {
       val userList = userRepository.findByUserId(user.userId)
        if(userList.isEmpty()) {
            return userRepository.save(user)
        }
        return null
    }

    fun deleteUser(id:Int) {
        return userRepository.deleteById(id)
    }

    fun updateUser(user: Users): Users {
        return userRepository.save(user)
    }

    fun getUser(id: Int?): Optional<Users> {
        if(id==null) {
            return Optional.empty()
        }
        return userRepository.findById(id)
    }

    fun getAllUsers(): MutableList<Users> {
        val userList: MutableList<Users> = mutableListOf()
        userRepository.findAll().forEach{userList.add(it)}
        return userList
    }

    fun authenticateUser(userId: String?, password: String?): Int? {
        return userRepository.validateUser(userId, password)
    }
}