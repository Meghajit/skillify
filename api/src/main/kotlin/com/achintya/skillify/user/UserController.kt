package com.achintya.skillify.user
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletResponse

@RestController
class UserController(val userService: UserService) {

    @GetMapping("/api/users")
    fun getAllUsers(response: HttpServletResponse): ResponseEntity<MutableList<Users>> {
        response.setHeader("Content-Type", "application/json")
        val listOfUsers = userService.getAllUsers()
        if(listOfUsers.size < 1) {
          return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
        }
        return ResponseEntity.ok(listOfUsers)
    }

    @PostMapping("/api/user")
    @ResponseStatus(HttpStatus.CREATED)
    fun addUser(@RequestBody user: Users,response: HttpServletResponse): ResponseEntity<Users> {
        response.setHeader("Content-Type", "application/json")
        val createdUser  = userService.addUser(user) ?: return ResponseEntity.status(HttpStatus.CONFLICT).build()
        return ResponseEntity.ok(createdUser)
    }

    @CrossOrigin
    @GetMapping("/api/user/{id}")
    fun getUser(@PathVariable id: Int, response: HttpServletResponse): ResponseEntity<Optional<Users>> {
        response.setHeader("Content-Type", "application/json")
        val user =  userService.getUser(id)
        if(user.isPresent) {
            return ResponseEntity.ok(user)
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @CrossOrigin
    @PostMapping("/api/user/auth")
    fun validateUser(@RequestBody user:Users, response: HttpServletResponse): ResponseEntity<UserIdentification> {
        response.setHeader("Content-Type", "application/json")
        val id =  userService.authenticateUser(user.userId, user.password)
        if(id!=null) {
            return ResponseEntity.ok(UserIdentification(id.toString()))
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PutMapping("/api/user")
    fun updateUser(@RequestBody user: Users, response: HttpServletResponse): ResponseEntity<Users> {
        response.setHeader("Content-Type", "application/json")
        if(userService.getUser(user.id).isPresent) {
            val updatedUser =  userService.updateUser(user)
            return ResponseEntity.ok(updatedUser)
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @DeleteMapping("/api/user/{id}")
    fun deleteUser(@PathVariable id:Int, response: HttpServletResponse) : ResponseEntity<Void> {
        response.setHeader("Content-Type", "application/json")
        if(userService.getUser(id).isPresent) {
            userService.deleteUser(id)
            return ResponseEntity.status(HttpStatus.OK).build()
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
}

data class UserIdentification(val id:String)